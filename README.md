# React QuiX Loader
Reduce the load of a website. If a media is already downloaded by another user, it will be downloader with webtorrent tech, so no load or GB consumed on the server machine. This will grant to the host that the website can be used by infinite people without bandwidth problem.

There is another way to improve this system (A wiki with all detail can be requested in the issue)

### Prop Types
Props to pass to the UI component

| Property | Type      | Required? | Description                                                                                               |
|:---------|:----------|:---------:|:----------------------------------------------------------------------------------------------------------|
| src      | String    |   true    | Like in an image component, the source of the file                                                        |
| infoHash | String    |   true    | An id to generate from https://instant.io                                                                 |
| loadingComponent | Component |   false   | A component waiting to find the torrent on p2p network                                                    |
| maxTimeout | Number    |   false   | Default 15000, it's the time that wait before load the content from the server instead of torrent network |
### Examples

Here is an example of the use of the component with the UI

```javascript
import {QuiXImg, QuiXVideo} from 'react-quix-loader';
import img1 from './asset/img1.jpg'
import videoSmall from './asset/video_small.mp4'

import img2 from './asset/img2.jpg'
import videoBig from './asset/video_big.mp4'

function App() {

    return (
        <div style={{display:"flex", flexDirection:"column"}}>
            Small image:
            <QuiXImg style={{height: "200px", objectFit:"contain"}} src={img1} infoHash={"befa0472c1d9fc266f0448f7b35076b4b29835c7"}/>
            Big image:
            <QuiXImg style={{height: "200px", objectFit:"contain"}} src={img2} infoHash={"b34630aa434af7a664d3a186b8fca7ce31734f09"}/>
            Small video:
            <QuiXVideo style={{height: "400px"}} src={videoSmall}
                       infoHash={"a8e5c780be600d02bac85a551bc8ed6626334dbf"}/>
            Big video:
            <QuiXVideo style={{height: "400px"}} src={videoBig}
                       infoHash={"cb9405f084dd224919f1399a425315c7a63211b9"}/>
        </div>
    );
}

export default App;
```


