import React, {useEffect, useState} from 'react';
import WebTorrent from 'webtorrent';
import {srcToFile, torrentOpts, trackerOpts} from "./utils";


export function QuiXImg(props) {
    let {src, infoHash, alt, maxTimeout, loadingComponent} = props;
    let [client, setClient] = useState(new WebTorrent({tracker: trackerOpts}));
    let [founded, setFounded] = useState(false);
    useEffect(() => {
        if (!src || !infoHash) {
        } else {
            let seedDirectFunction = () => {
                let img = document.getElementById(infoHash)
                if (!img.src) {
                    srcToFile(src, src.replace(/^.*[\\\/]/, '')).then((file) => {
                        client.remove(infoHash);
                        client.seed(file, torrentOpts, (t) => {
                            console.log("Image " + infoHash + " rendered from server network ")
                            t.files[0].renderTo(img)
                            setFounded(true);
                        })
                    })
                }
            }
            client.add(infoHash, torrentOpts, (torrent) => {
                let img = document.getElementById(infoHash)
                if (!img.src) {
                    torrent.files[0].renderTo(img)
                    console.log("Image " + infoHash + " rendered from torrent network")
                    setFounded(true);
                }
            })
            setTimeout(seedDirectFunction, maxTimeout)
        }
    }, [])

    if (!src || !infoHash) {
        if (alt) {
            return <>{alt}</>
        } else {
            return <>Image unavailable</>
        }
    }

    return (
        <>
            {!founded && loadingComponent ? loadingComponent : null}
            <img {...props} id={infoHash} src={undefined} maxTimeout={undefined} infoHash={undefined}/>
        </>
    );
}

QuiXImg.defaultProps = {
    maxTimeout: 15000,
    loadingComponent: undefined
}
