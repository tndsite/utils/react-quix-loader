import axios from "axios";

export const srcToFile = async (src, fileName) => {
    const response = await axios.get(src, {
        responseType: "blob",
    });
    const mimeType = response.headers["content-type"];
    let splinted = fileName.split(".");
    splinted.splice(splinted.length - 2, 1);
    fileName = splinted.join(".");
    return new File([response.data], fileName, {type: mimeType});
};
const rtcConfig = () => {
    if (process.env.REACT_APP_SECRET_CODE) {
        return {
            "iceServers": [
                {
                    "urls": "stun:23.21.150.121"
                },
                {
                    urls: [
                        'stun:stun.l.google.com:19302',
                        'stun:global.stun.twilio.com:3478'
                    ]
                },
                {
                    "username": "admin",
                    "credential": process.env.REACT_APP_SECRET_CODE,
                    "urls": "turn:185.149.22.163:3478"
                },
                {
                    "username": "admin",
                    "credential": process.env.REACT_APP_SECRET_CODE,
                    "urls": "turn:23.94.202.235:3478"
                }
            ]
        }
    } else {
        return {
            "iceServers": [
                {
                    "urls": "stun:23.21.150.121"
                },
                {
                    urls: [
                        'stun:stun.l.google.com:19302',
                        'stun:global.stun.twilio.com:3478'
                    ]
                }
            ]
        }
    }
}
const trackers = ['wss://tracker.btorrent.xyz', 'wss://tracker.openwebtorrent.com', 'wss://tracker.quix.cf', 'wss://tracker.crawfish.cf']
export const torrentOpts = {
    announce: trackers
}
export const trackerOpts = {
    announce: trackers,
    rtcConfig: rtcConfig()
}
