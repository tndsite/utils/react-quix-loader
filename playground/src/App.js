import {QuiXImg, QuiXVideo} from 'react-quix-loader';
import img1 from './asset/img1.jpg'
import videoSmall from './asset/video_small.mp4'

import img2 from './asset/img2.jpg'
import videoBig from './asset/video_big.mp4'

function App() {

    return (
        <div style={{display:"flex", flexDirection:"column"}}>
            Small image:
            <QuiXImg style={{height: "200px", objectFit:"contain"}} src={img1} infoHash={"befa0472c1d9fc266f0448f7b35076b4b29835c7"}/>
            Big image:
            <QuiXImg style={{height: "200px", objectFit:"contain"}} src={img2} infoHash={"b34630aa434af7a664d3a186b8fca7ce31734f09"}/>
            Small video:
            <QuiXVideo style={{height: "400px"}} src={videoSmall}
                       infoHash={"a8e5c780be600d02bac85a551bc8ed6626334dbf"}/>
            Big video:
            <QuiXVideo style={{height: "400px"}} src={videoBig}
                       infoHash={"cb9405f084dd224919f1399a425315c7a63211b9"}/>
        </div>
    );
}

export default App;
